import React, { useEffect, useState } from 'react';
import './App.css';
import { Route,Switch } from "react-router-dom";
import Home from './components/Home';
import Listwithposter from './components/Listwithposter';
import List from './components/List';
function App() {
  return (
    <div className="App">
      <Switch>
      <Route  path='/poster' component ={Listwithposter}></Route>
      <Route  path='/list' component ={List}></Route>
      <Route path='/' component ={Home}></Route>
      </Switch>
    </div>
  );
}

export default App;
