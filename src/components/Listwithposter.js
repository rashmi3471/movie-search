import React, { useState, useRef } from 'react';
import '../App.css';
import { Card, CardDeck } from 'react-bootstrap';

function Posterlist() {
    const ctitle = useRef('');
    const cyear = useRef('');
    const [enteredmovie, setEnteredMovie] = useState();
    const [enteredyear, setEnteredyear] = useState();
    const [movies, setMovies] = useState('');

    const getMovieDeatails = (e) => {
        e.preventDefault();
        if (enteredmovie !== '' && enteredyear !== '') {
            fetch(`http://www.omdbapi.com/?s=${enteredmovie}&y=${enteredyear}&apikey=b2d58b90`)
                .then(response => response.json())
                .then(data => {
                    setMovies(data.Search);
                });
        }

    }

    let data;
    if (movies) {
        data = movies.map(
            (val, index) => {
                return (
                    <Card>
                        <Card.Img variant="top" src={val.Poster} />
                        <Card.Body>
                            <Card.Title>{val.Title}</Card.Title>
                            <Card.Text>
                                <div>Type:{val.Type}</div>
                                <div>Year:{val.Year}</div>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                )
            }
        )
    }
    return (
        <div>
            <a href="/">Back</a> <br></br>
            <form className={'form-cls'} onSubmit={getMovieDeatails}>
                <input ref={ctitle} type='text' onChange={(e) => setEnteredMovie(e.target.value.trim())} placeholder='Enter Movie Title'></input>
                <input ref={cyear} type='text'
                    onChange={() => setEnteredyear(cyear.current.value.trim())}
                    placeholder='Enter Movie Release Year'></input>
                <button type='submit'>Submit</button>
            </form>
            <CardDeck>{data}</CardDeck>
        </div>

    );
}
export default Posterlist;