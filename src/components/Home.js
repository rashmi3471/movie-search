import React from 'react';
// import '../App.css';

function Home(props) {
    const loadMovieWithImage =()=>{
        props.history.push( "/poster");
    }
  const  loadMovieDetails = () =>{
    props.history.push( "/list")
    }
  return (
    <div className="App">
      <button className='movie_details' onClick={loadMovieDetails}>Movie Details</button>
      <button className='movie_details' onClick={loadMovieWithImage}>Movie Details With Poster</button>
    </div>
  );
}

export default Home;
