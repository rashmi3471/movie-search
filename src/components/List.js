import React, { useState, useRef } from 'react';
import '../App.css';
import { Button, Card, CardDeck, Pagination } from 'react-bootstrap';
import Modalblock from './Modalblock';
function List() {
    const ctitle = useRef('');
    const cyear = useRef('');
    const [enteredmovie, setEnteredMovie] = useState();
    const [enteredyear, setEnteredyear] = useState();
    const [modalData, setModalData] = useState('');
    const [movies, setMovies] = useState('');

    const [modalShow, setModalShow] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);

    const getMovieDeatails = (e) => {
        e.preventDefault();
        if (typeof enteredmovie !== 'undefined' && typeof enteredyear !== 'undefined') {
            fetch(`http://www.omdbapi.com/?s=${enteredmovie.trim()}&y=${enteredyear.trim()}&apikey=b2d58b90`)
                .then(response => response.json())
                .then(data => {
                    setMovies(data.Search);
                    console.log(data);
                });
        }
    }
    const getMoredetails = (imdbID) => {
        fetch(`http://www.omdbapi.com/?i=${imdbID}&plot=full&apikey=b2d58b90`)
            .then(response => response.json())
            .then(data => {
                setModalData(data);
                setModalShow(true);
            });
    }
    let data, paginate, modal;
    if (movies) {
        data = movies.slice(currentPage -1, currentPage).map(
            (val, index) => {
                return (
                    <Card style={{ width: '18rem' }} className='poster-div' key={index}>
                        <Card.Body>
                            <div>Title:{val.Title}</div>
                            <div>Type:{val.Type}</div>
                            <div>Year:{val.Year}</div>
                            <Button variant="primary" onClick={() => getMoredetails(val.imdbID)}>More</Button>
                        </Card.Body>
                    </Card>
                )
            }
        )
        let items = [];
        for (let number = 1; number <= movies.length; number++) {
            items.push(
                <Pagination.Item key={number} active={number === currentPage} onClick={e => setCurrentPage(number)}>
                    {number}
                </Pagination.Item>,
            );
        }
        paginate = (
            <div>
                <Pagination>{items}</Pagination>
            </div>
        );

    }
    if (modalData) {
        modal = <Modalblock
            infomation={modalData}
            show={modalShow}
            onHide={() => setModalShow(false)}
        />
    }
    return (
        <div>
            <a href="/">Back</a> <br></br>
            <form className={'form-cls'} onSubmit={getMovieDeatails}>
                <input ref={ctitle} type='text' onChange={(e) => setEnteredMovie(e.target.value.trim())} placeholder='Enter Movie Title'></input>
                <input ref={cyear} type='text'
                    onChange={() => setEnteredyear(cyear.current.value.trim())}
                    placeholder='Enter Movie Release Year'></input>
                <button type='submit'>Submit</button>

            </form>
            <CardDeck>{data}</CardDeck>
            {paginate}
            {modal}
        </div>

    );
}
export default List;