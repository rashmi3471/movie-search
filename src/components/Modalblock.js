import React from 'react';
import { Modal, Button } from 'react-bootstrap';

function Modalblock(props) {
  const data = Object.keys(props.infomation).map(function (key, index) {
    if (typeof props.infomation[key] === 'object') {
      return false;
    }
    return <li key={key}><strong>{key}</strong>: {props.infomation[key]}</li>
  }).filter(function (value) { return (value) });

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.infomation.Title}
          <p>boxoffice: {props.infomation.imdbRating > 7 ? 'Hit' : 'Flop'}</p>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>{data}</ul>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}
export default Modalblock;